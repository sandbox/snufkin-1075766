<?php

/**
 * Expose the redirect pages where there is a ?destination= query as a context condition.
 */
class context_destination_context_condition_destination extends context_condition_path {
  /**
  * Execute.
  */
  function execute() {
    if ($this->condition_used()) {
      // Include both the path alias and normal path for matching.
      $current_path = array(drupal_get_path_alias($_GET['destination']));
      if ($current_path != $_GET['destination']) {
        $current_path[] = $_GET['destination'];
      }
      foreach ($this->get_contexts() as $context) {
        $paths = $this->fetch_from_context($context, 'values');
        if ($this->match($current_path, $paths, TRUE)) {
          $this->condition_met($context);
        }
      }
    }
  }
}